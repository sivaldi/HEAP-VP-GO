# VP-GO:  a ``light'' action-conditioned visual prediction model

This repository contains the official TensorFlow 2 implementation of VP-GO: a ``light'' action-conditioned visual prediction model (under review ICRA 2022).

### What is VP-GO?

VP-GO is an algorithm for training models for visual prediction for robot grasping.
We proposed a hierarchical decomposition of semantic grasping and manipuation actions into elementary end-effector movements, to ensure compatibility with existing models (e.g., GHVAE) and re-use existing datasets (e.g., RoboNet).

VP-GO is a VAE-based, action conditional stochastic visual prediction model that is specific to deal with high stochasticity such as in robotic grasping unknown objects in a heap. Our model is inspired by SVG-LP and its successors SVG’ and GHVAE, but it is “lighter” than those, in that it only contains simple convolutional and recurrent networks and has a smaller number of parameters to train. Furthermore, it is an action conditional model that can deal with semantic action description. 


<!-- ## (I put all my original stuff here, I I am continuing to clean...) -->

## Setup

### Install dependencies

To install all the dependencies, please run the following:
```
pip install -r requirements.txt
```

### Clone and install this repository
```
git clone https://gitlab.inria.fr/sivaldi/HEAP-VP-GO.git
cd HEAP-VP-GO
pip install -e .
```

## Prepare Dataset
Our models can be trained with the (big) RoboNet dataset and our (smaller) PandaGrasp dataset.
Note that RoboNet only contains elementary movements, while PandaGrasp contains both elementary movements and semantic information about the actions.

### RoboNet dataset
Please follow the official instruction to download [RoboNet](https://github.com/SudeepDasari/RoboNet/wiki/Getting-Started)

### PandaGrasp dataset
PandaGrasp is a real robot dataset we released with sequences of semantic actions defined per our hierarchy and recorded frames, executed by a Franka robot. Please download it from our [PandaGrasp](https://gitlab.inria.fr/sivaldi/PandaGrasp_dataset.git) repository.


## Inference
You can download our pre-trained models for [RoboNet](https://mybox.inria.fr/smart-link/5ec1bba0-66f0-4c11-a0bd-761e1853d11b/) and [PandaGrasp](https://mybox.inria.fr/smart-link/baa6eef4-7150-42e7-8991-110841168c72/) datasets.
```
python scripts/inference.py hparams/robonet.yaml --out_dir logs --sample_dir samples --restore_path 0620-230005 
```
```
python scripts/inference.py hparams/pandagrasp.yaml --out_dir logs --sample_dir samples --restore_path 0624-222352
```

## Training
Here we provide examples for training on different datasets. Note that it is always possible to fine-tune a model trained on a different dataset. In our paper, we trained the models on RoboNet then fine-tuned on PandaGrasp.

### From scrach on Robonet dataset
```
python scripts/train.py hparams/robonet.yaml --out_dir logs --enable_function
```
### Fine-tune on PandaGrasp dataset
```
python scripts/train.py hparams/pandagrasp.yaml --out_dir logs --restore_path 0620-230005 --finetune
```
## Benchmark
<img src='images/banchmark1.png' align="left" width=650>
<br><br><br><br><br><br><br><br><br>
<img src='images/banchmark2.png' align="left" width=650>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>

## Acknowledgments
HEAP project
