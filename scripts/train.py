import os
import re
import copy
import glob
import yaml
import datetime
import argparse
import numpy as np
import tensorflow as tf
from collections import OrderedDict
from tensor2tensor.utils.hparam import HParams

from vpgo.datasets import get_dataset
from vpgo.models import get_model_class
from vpgo.utils.utils import load_hparams, to_images, to_gifs

class Trainer(object):
    def __init__(self, strategy, loader, hparams_dict, args, **hparams):
        self.strategy = strategy
        self.args = args
        self.yaml_file = hparams_dict.pop('yaml_file', '')
        self.hparams = self.parse_hparams(hparams_dict, hparams)
        self.build_datasets(loader)
        self.path = self.restore(args.restore_path)
        self.summary_writer = tf.summary.create_file_writer(self.path)

    def build_datasets(self, loader):
        if isinstance(loader, list):            
            train_dataset = tf.data.Dataset.from_generator(
                lambda: loader[0], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32)]))
            val_dataset = tf.data.Dataset.from_generator(
                lambda: loader[1], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32)]))
            test_dataset = tf.data.Dataset.from_generator(
                lambda: loader[2], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32)]))
            self.dataset_hparams = loader[0].hparams
        else:
            train_dataset = tf.data.Dataset.from_generator(
                lambda: loader.generators['train'], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32), ('f_names', tf.string)]))
            val_dataset = tf.data.Dataset.from_generator(
                lambda: loader.generators['val'], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32), ('f_names', tf.string)]))
            test_dataset = tf.data.Dataset.from_generator(
                lambda: loader.generators['test'], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32), ('f_names', tf.string)]))
            self.dataset_hparams = loader.hparams

        self.train_dist_dataset = self.strategy.experimental_distribute_dataset(train_dataset)
        self.val_dist_dataset = self.strategy.experimental_distribute_dataset(val_dataset)
        

    def parse_hparams(self, hparams_dict, hparams):
        self.model_params = hparams_dict.pop('model_params', {})
        deprecated_hparams_keys = [
        ]
        for deprecated_hparams_key in deprecated_hparams_keys:
            hparams_dict.pop(deprecated_hparams_key, None)

        parsed_hparams = self.get_default_hparams().override_from_dict(hparams_dict or {})
        if hparams:
            if not isinstance(hparams, (list, tuple)):
                hparams = [hparams]
            for hparam in hparams:
                parsed_hparams.parse(hparam)
        return parsed_hparams

    def get_default_hparams(self):
        return HParams(**self.get_default_hparams_dict())

    def get_default_hparams_dict(self):
        hparams = dict(
            model='vpgo',
            lr=0.001,
            end_lr=1e-8,
            decay_steps=[200000, 400000],
            lr_boundaries=[0,],
            beta1=0.9,
            beta2=0.999,
            start_step = 0,
            max_steps=400000,
            scalar_summary_freq=100,
            image_summary_freq=10000,
            eval_freq=10000,
            save_freq=5000,
        )
        return hparams

    def restore(self, restore_path):
        VideoPredictionModel = get_model_class(self.hparams.model)
        self.model = VideoPredictionModel(hparams_dict=self.model_params, batch_size=self.args.batch_size//self.args.n_gpus, n_gpus=self.args.n_gpus)
        self.model_hparams = copy.deepcopy(self.model.hparams)
        self.optimizer = {'optimizer': tf.keras.optimizers.Adam(learning_rate=self.hparams.lr, beta_1=self.hparams.beta1, beta_2=self.hparams.beta2)}
        
        if restore_path:
            assert os.path.exists(os.path.join(self.args.out_dir, restore_path))
            restore_path = os.path.join(self.args.out_dir, restore_path)
            print('restore_path: ', restore_path)
            with open(restore_path + '/config.yaml') as f:
                config = yaml.safe_load(f)
            ckpts = sorted(glob.glob(restore_path + '/checkpoints/ckpt-*.index'))
            if len(ckpts) > 0:
                if not self.args.finetune:
                    # reader = tf.train.load_checkpoint(re.split('.index', ckpts[-1])[0])
                    # shape_from_key = reader.get_variable_to_shape_map()
                    # nets = set([re.split('/', k)[1] for k in shape_from_key.keys() if len(re.split('/', k)) > 0 and re.split('/', k)[0]=='model'])
                    fake_model_hp = dict(
                        decoder=self.model.decoder,
                        encoder=self.model.encoder,
                        frame_predictor=self.model.frame_predictor,
                        posterior=self.model.posterior,
                        prior=self.model.prior,
                        enc_action_append=self.model.enc_action_append,
                        enc_action_embed=self.model.enc_rnn_embed_prior,
                        enc_action_embed_pred=self.model.enc_rnn_embed_pred
                    )
                    fake_model = tf.train.Checkpoint(**fake_model_hp)
                    checkpoint_tmp = tf.train.Checkpoint(model=fake_model)            
                    # checkpoint_tmp = tf.train.Checkpoint(model=self.model)
                    self.status = checkpoint_tmp.restore(re.split('.index', ckpts[-1])[0])                
                    self.checkpoint_number = int(re.split('ckpt-|.index', ckpts[-1])[-2])
                    global_step = tf.compat.v1.train.get_or_create_global_step()
                    self.hparams.start_step = config['save_freq']*(int(re.split('ckpt-|.index', ckpts[-1])[-2])-1)+1
                    global_step = tf.compat.v1.assign_add(global_step, self.hparams.start_step)
                else:
                    fake_model_hp = dict(
                        decoder=self.model.decoder,
                        encoder=self.model.encoder,
                        frame_predictor=self.model.frame_predictor,
                        posterior=self.model.posterior,
                        prior=self.model.prior,
                        enc_action_embed=self.model.enc_rnn_embed_prior,
                        enc_action_embed_pred=self.model.enc_rnn_embed_pred
                    )
                    fake_model = tf.train.Checkpoint(**fake_model_hp)
                    checkpoint_tmp = tf.train.Checkpoint(model=fake_model)
                    self.status = checkpoint_tmp.restore(re.split('.index', ckpts[-1])[0])
                    self.checkpoint_number = 0
            else:
                raise ValueError
        else:
            self.checkpoint_number = 0

        current_time = datetime.datetime.now().strftime("%m%d-%H%M%S")
        if restore_path and not self.args.finetune:                  
            path_n = restore_path
        else:
            path_n = self.args.out_dir + '/' + current_time
            
        self.checkpoint = tf.train.Checkpoint(model=self.model)
        self.manager = tf.train.CheckpointManager(self.checkpoint, directory=path_n+'/checkpoints', max_to_keep=1)
        
        return path_n

    def schedule_lr(self):        
        global_step = tf.compat.v1.train.get_or_create_global_step()
        if any(self.hparams.lr_boundaries):
            lr_values = list(self.hparams.lr * 0.1 ** np.arange(len(self.hparams.lr_boundaries) + 1))
            learning_rate = tf.compat.v1.train.piecewise_constant(global_step, self.hparams.lr_boundaries, lr_values)
        elif any(self.hparams.decay_steps):
            start_step, end_step = self.hparams.decay_steps
            if start_step == end_step:
                schedule = tf.cond(tf.less(global_step, start_step),
                                    lambda: 0.0, lambda: 1.0)
            else:
                step = tf.clip_by_value(global_step, start_step, end_step)
                schedule = tf.compat.v1.to_float(step - start_step) / tf.compat.v1.to_float(end_step - start_step)
            learning_rate = self.hparams.lr + (self.hparams.end_lr - self.hparams.lr) * schedule
        else:
            learning_rate = tf.constant(self.hparams.lr)
        return learning_rate

    def train_step(self, inputs):
        with tf.GradientTape() as gen_tape:
            outputs_dict = self.model(inputs, training=True)
            loss_gen = outputs_dict['loss_gen']
        
        learning_rate = self.schedule_lr()
        g_gradients = gen_tape.gradient([loss_gen], self.model.g_vars)
        self.optimizer['optimizer'].learning_rate = learning_rate
        self.optimizer['optimizer'].apply_gradients(zip(g_gradients, self.model.g_vars))
       
        return outputs_dict, learning_rate
    
    def distributed_train_step(self, dataset_inputs):
        outputs_dicts, learning_rate = self.strategy.run(self.train_step, args=(dataset_inputs,))
        losses = OrderedDict([(key, self.strategy.reduce(tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None))
                              for key, per_replica_losses in outputs_dicts.items() if 'loss' in key])

        outputs = OrderedDict([(key, tf.concat(self.strategy.experimental_local_results(output), axis=0)
                                if len(self.strategy.experimental_local_results(output)[0].shape.as_list()) > 0
                                else self.strategy.reduce(tf.distribute.ReduceOp.MEAN, output, axis=None))
                               for key, output in outputs_dicts.items() if 'loss' not in key])
                                 
        learning_rate = self.strategy.experimental_local_results(learning_rate)
        return outputs, losses, learning_rate
    
    def test_step(self, inputs):
        outputs_dict = self.model(inputs, training=False)
        return outputs_dict

    def distributed_test_step(self, dataset_inputs):
        outputs_dicts = self.strategy.run(self.test_step, args=(dataset_inputs,))
        losses = OrderedDict([(key, (self.strategy.reduce(tf.distribute.ReduceOp.SUM, per_replica_losses, axis=None)))
                              for key, per_replica_losses in outputs_dicts.items() if 'loss' in key])

        outputs = OrderedDict([(key, tf.concat(self.strategy.experimental_local_results(output), axis=0)
                                if len(self.strategy.experimental_local_results(output)[0].shape.as_list()) > 0
                                else tf.stack(self.strategy.experimental_local_results(output)))
                               for key, output in outputs_dicts.items() if 'loss' not in key])
        return outputs, losses

    def update_global_step(self):
        global_step = tf.compat.v1.train.get_or_create_global_step()
        global_step = tf.compat.v1.assign_add(global_step, 1)
    
    def custom_loop(self):
        if self.args.enable_function:
            self.distributed_train_step = tf.function(self.distributed_train_step)      
            self.distributed_test_step = tf.function(self.distributed_test_step)
            self.update_global_step = tf.function(self.update_global_step)

        val_iter = iter(self.val_dist_dataset)

        start_time = datetime.datetime.now()
        for step_n, x in enumerate(self.train_dist_dataset):
            step = self.hparams.start_step + step_n
            print('step --> {}'.format(step))
            if step > self.hparams.max_steps:
                break
            outputs, losses, learning_rate = self.distributed_train_step(x)
            if step % self.hparams.scalar_summary_freq == 0:
                x_val = next(val_iter)
                outputs_val, losses_val = self.distributed_test_step(x_val)           
            
            step_time = datetime.datetime.now() - start_time
            start_time = datetime.datetime.now()
            with self.summary_writer.as_default():
                tf.summary.scalar('step_time', step_time.total_seconds(), step=step)
                tf.summary.scalar('learning_rate', learning_rate[0].numpy(), step=step)
                if step % self.hparams.scalar_summary_freq == 0:
                    # summary losses
                    for key, loss in losses.items():
                        if key in losses_val:
                            tf.summary.scalar(key + '/train', loss.numpy(), step=step)
                            tf.summary.scalar(key + '/val', losses_val[key].numpy(), step=step)
                        else:
                            tf.summary.scalar(key, loss.numpy(), step=step)
                    # summary outputs (gt_sampling_mean)
                    for key, tensor in outputs.items():
                        if key == 'gt_sampling_mean':
                            tf.summary.scalar(key, tensor.numpy(), step=step)

                if step % self.hparams.image_summary_freq == 0:
                    # summary outputs
                    for key, tensor in outputs.items():
                        if key != 'images':
                            continue
                        assert key in outputs_val
                        tf.summary.image(key + '/train', to_images(tensor, self.model.hparams.image_size, self.model.hparams.get('context_frames', 0)), step=step)
                        tf.summary.image(key + '/val', to_images(outputs_val[key], self.model.hparams.image_size, self.model.hparams.get('context_frames', 0)), step=step)
                        summ = tf.compat.v1.Summary()
                        summ.value.add(tag=key + '_gif/train', image=to_gifs(tensor, self.model.hparams.image_size, self.model.hparams.get('context_frames', 0)))
                        tf.summary.experimental.write_raw_pb(summ.SerializeToString(), step=step)
                        summ = tf.compat.v1.Summary()
                        summ.value.add(tag=key + '_gif/val', image=to_gifs(outputs_val[key], self.model.hparams.image_size, self.model.hparams.get('context_frames', 0)))
                        tf.summary.experimental.write_raw_pb(summ.SerializeToString(), step=step)
                self.summary_writer.flush()
                  
            self.update_global_step()

            if step % self.hparams.save_freq == 0:
                self.checkpoint_number += 1
                self.manager.save(checkpoint_number=self.checkpoint_number)
            
            if step == 0:
                model_params = self.model.hparams.values()
                with open(os.path.join(self.path, 'model_hparams.yaml'), 'w') as f:
                    yaml.dump(model_params, f)
                with open(os.path.join(self.path, 'dataset_hparams.yaml'), 'w') as f:
                    yaml.dump(self.dataset_hparams.values(), f)
                with open(os.path.join(self.path, 'config.yaml'), 'w') as f:
                    yaml.dump(self.hparams.values(), f)
            

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('hparams_file', type=str, help='path to YAML config file')
    parser.add_argument('--out_dir', type=str, default='', help='out dir')
    parser.add_argument('--restore_path', type=str, default='', help='restore path')
    parser.add_argument('--finetune', action='store_true', help='finetune')
    parser.add_argument('--enable_function', action='store_true', help='enable function')
    parser.add_argument('--n_gpus', type=int, default=4, help='number of gpus')
    parser.add_argument('--batch_size', type=int, default=32, help='batch size')    
    args = parser.parse_args()

    physical_devices = tf.config.list_physical_devices('GPU')
    for device in physical_devices:
        tf.config.experimental.set_memory_growth(device, True)

    if not os.path.exists(args.out_dir):
        os.makedirs(args.out_dir)

    hparams_dict = load_hparams(args.hparams_file)
    physical_devices = tf.config.list_physical_devices('GPU')
    
    assert len(physical_devices) >= args.n_gpus
    assert args.batch_size % args.n_gpus == 0
    devices = []
    for i in range(args.n_gpus):
        devices.append('/gpu:{}'.format(i))            
    strategy = tf.distribute.MirroredStrategy(devices=devices)

    loader = get_dataset(dataset=hparams_dict.pop('dataset', 'RoboNet'),
                         batch_size=args.batch_size,
                         hparams=hparams_dict.pop('dataset_params', dict()))

    with strategy.scope():
        trainer = Trainer(strategy, loader, hparams_dict, args)
        trainer.custom_loop()


    
    
