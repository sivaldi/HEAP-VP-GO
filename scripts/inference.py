import os
import re
import copy
import glob
import argparse
import tensorflow as tf
from collections import OrderedDict

from vpgo.datasets import get_dataset
from vpgo.models.pred_model import PredModel
from vpgo.utils.utils import load_hparams, to_images, save_gifs

class Inference(object):
    def __init__(self, strategy, loader, hparams_dict, args, **hparams):
        self.strategy = strategy
        self.args = args
        self.yaml_file = hparams_dict.pop('yaml_file', '')
        self.build_datasets(loader)
        self.path = self.restore(args.restore_path)

    def build_datasets(self, loader):
        if isinstance(loader, list):            
            train_dataset = tf.data.Dataset.from_generator(
                lambda: loader[0], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32)]))
            val_dataset = tf.data.Dataset.from_generator(
                lambda: loader[1], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32)]))
            test_dataset = tf.data.Dataset.from_generator(
                lambda: loader[2], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32)]))
            self.dataset_hparams = loader[0].hparams
        else:
            train_dataset = tf.data.Dataset.from_generator(
                lambda: loader.generators['train'], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32), ('f_names', tf.string)]))
            val_dataset = tf.data.Dataset.from_generator(
                lambda: loader.generators['val'], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32), ('f_names', tf.string)]))
            test_dataset = tf.data.Dataset.from_generator(
                lambda: loader.generators['test'], OrderedDict([('images', tf.float32), ('actions', tf.float32), ('states', tf.float32), ('f_names', tf.string)]))
            self.dataset_hparams = loader.hparams

        self.test_dist_dataset = self.strategy.experimental_distribute_dataset(test_dataset)
        self.dataset_hparams = loader[0].hparams

    def restore(self, restore_path):
        self.model_params = hparams_dict.pop('model_params', {})
        self.model = PredModel(hparams_dict=self.model_params, batch_size=self.args.batch_size//self.args.n_gpus, n_gpus=self.args.n_gpus)
        self.model_hparams = copy.deepcopy(self.model.hparams)
        assert restore_path
        assert os.path.exists(os.path.join(self.args.out_dir, restore_path))
        restore_path = os.path.join(self.args.out_dir, restore_path)
        print('restore_path: ', restore_path)
        ckpts = sorted(glob.glob(restore_path + '/checkpoints/ckpt-*.index'))
        if len(ckpts) > 0:
            checkpoint_tmp = tf.train.Checkpoint(model=self.model)
            self.status = checkpoint_tmp.restore(re.split('.index', ckpts[-1])[0])
        else:
            raise ValueError
       
        return restore_path

    def eval_sample(self, inputs):
        outputs = self.model.evaluate_sample(inputs)
        return outputs
    
    def distributed_eval_sample(self, dataset_inputs):
        outputs = self.strategy.run(self.eval_sample, args=(dataset_inputs,))
        outputs['scale'] = OrderedDict([(key, tf.concat(self.strategy.experimental_local_results(
            output), axis=0) if len(self.strategy.experimental_local_results(output)[0].shape.as_list()) > 0
            else tf.stack(self.strategy.experimental_local_results(output))) for key, output in outputs['scale'].items()])
        outputs['tensor'] = OrderedDict([(key, tf.concat(self.strategy.experimental_local_results(
            output), axis=0) if len(self.strategy.experimental_local_results(output)[0].shape.as_list()) > 0
            else tf.stack(self.strategy.experimental_local_results(output))) for key, output in outputs['tensor'].items()])
        return outputs

    def batch(self):
        if self.args.enable_function:
            self.distributed_eval_sample = tf.function(self.distributed_eval_sample)

        test_iter = iter(self.test_dist_dataset)
        x_test = next(test_iter)
        metrics_eval = self.distributed_eval_sample(x_test)

        images = to_images(metrics_eval['tensor']['lpips'][:, :, :96],
                      self.model.hparams.image_size, self.model.hparams.get('context_frames', 0))

        save_gifs(self.args.sample_dir + '/' + self.args.restore_path , images)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('hparams_file', type=str, help='path to YAML config file')
    parser.add_argument('--out_dir', type=str, default='', help='out dir')
    parser.add_argument('--sample_dir', type=str, default='', help='out dir')
    parser.add_argument('--restore_path', type=str, default='', help='restore path')
    parser.add_argument('--enable_function', action='store_true', help='enable function')
    parser.add_argument('--n_gpus', type=int, default=1, help='number of gpus')
    parser.add_argument('--batch_size', type=int, default=16, help='batch size')
    args = parser.parse_args()

    physical_devices = tf.config.list_physical_devices('GPU')
    for device in physical_devices:
        tf.config.experimental.set_memory_growth(device, True)

    if not os.path.exists(args.sample_dir):
        os.makedirs(args.sample_dir)

    hparams_dict = load_hparams(args.hparams_file)
    physical_devices = tf.config.list_physical_devices('GPU')
    
    assert len(physical_devices) >= args.n_gpus
    assert args.batch_size % args.n_gpus == 0
    devices = []
    for i in range(args.n_gpus):
        devices.append('/gpu:{}'.format(i))            
    strategy = tf.distribute.MirroredStrategy(devices=devices)

    loader = get_dataset(dataset=hparams_dict.pop('dataset', 'RoboNet'),
                         batch_size=args.batch_size,
                         hparams=hparams_dict.pop('dataset_params', dict()))

    with strategy.scope():
        inference = Inference(strategy, loader, hparams_dict, args)
        inference.batch()


    
    
