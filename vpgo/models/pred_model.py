import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa
import tensorflow.keras.layers as layers
from collections import OrderedDict
from vpgo.models.base_model import BaseModel

def get_norm_layer(norm_layer):
    op = None
    if norm_layer == 'batch':
        op = layers.BatchNormalization(axis=-1,
                                       gamma_initializer=tf.keras.initializers.RandomNormal(mean=1., stddev=0.02)
                                       )
    elif norm_layer == 'sync_batch':
        op = tf.keras.layers.experimental.SyncBatchNormalization(axis=-1,
                                                                 gamma_initializer=tf.keras.initializers.RandomNormal(mean=1., stddev=0.02)
                                                                 )
    elif norm_layer == 'instance':
        op = tfa.layers.InstanceNormalization(axis=-1, center=True, scale=True,
                                              gamma_initializer=tf.keras.initializers.RandomNormal(mean=1., stddev=0.02)
                                              )
    else:
        if norm_layer != 'none':
            raise NotImplementedError
    return op

def get_upsampling(size, up_layer):
    up1 = tf.keras.Sequential()
    if up_layer == 'upsampling':
        up1.add(layers.UpSampling2D(size=size))
    elif up_layer == 'transpose':
        raise NotImplementedError
        # up1.add(layers.Conv2DTranspose(filters==hparams.lstm_filter, kernel_size=size, padding='valid',
        #                             kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)
        #                             ))
        # up1.add(get_norm_layer(norm_layer))
        # up1.add(layers.LeakyReLU(0.2))
    return up1

class VGGLayer(tf.keras.Model):
    def __init__(self, nout, kernel_size, norm_layer, activation, padding='same'):
        super(VGGLayer, self).__init__()
        self.norm_layer = norm_layer
        self.activation = activation
        self.conv = layers.Conv2D(filters=nout,
                                  kernel_size=kernel_size,
                                  strides=(1, 1),
                                  padding=padding,
                                  kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)
                                  )
        self.norm = get_norm_layer(norm_layer)
        if activation == 'leakyrelu':
            self.relu = layers.LeakyReLU(0.2)
        else:
            if activation != 'none':
                raise NotImplementedError
            
    def call(self, x, training):
        h = self.conv(x)
        if self.norm_layer != 'none':
            if 'batch' in self.norm_layer:
                h = self.norm(h, training=training)
            else:
                h = self.norm(h)
        if self.activation == 'none':
            return h
        else:
            return self.relu(h)

class VGGEncoder(tf.keras.Model):
    def __init__(self, hparams):
        super(VGGEncoder, self).__init__()
        self.hparams = hparams
        filters = hparams.enc_filters
        norm_layer = hparams.norm_layer

        self.ops = []
        self.mps = []
        if len(filters) > 0:  # 64 x 64 -> 32 x 32 // 128 x 128 -> 64 x 64
            c1 = tf.keras.Sequential()
            c1.add(VGGLayer(filters[0], 3, norm_layer, 'leakyrelu'))
            c1.add(VGGLayer(filters[0], 3, norm_layer, 'leakyrelu'))
            self.ops.append(c1)
            self.mps.append(layers.MaxPool2D(pool_size=2, strides=2, padding='valid'))
        if len(filters) > 1:  # 32 x 32 -> 16 x 16 // 64 x 64 -> 32 x 32
            c2 = tf.keras.Sequential()
            c2.add(VGGLayer(filters[1], 3, norm_layer, 'leakyrelu'))
            c2.add(VGGLayer(filters[1], 3, norm_layer, 'leakyrelu'))
            self.ops.append(c2)
            self.mps.append(layers.MaxPool2D(pool_size=2, strides=2, padding='valid'))
        if len(filters) > 2:  # 16 x 16 -> 8 x 8  // 32 x 32 -> 16 x 16
            c3 = tf.keras.Sequential()
            c3.add(VGGLayer(filters[2], 3, norm_layer, 'leakyrelu'))
            c3.add(VGGLayer(filters[2], 3, norm_layer, 'leakyrelu'))
            c3.add(VGGLayer(filters[2], 3, norm_layer, 'leakyrelu'))
            self.ops.append(c3)
            self.mps.append(layers.MaxPool2D(pool_size=2, strides=2, padding='valid'))
        if len(filters) > 3:  # 8 x 8 -> 4 x 4    // 16 x 16 -> 8 x 8
            c4 = tf.keras.Sequential()
            c4.add(VGGLayer(filters[3], 3, norm_layer, 'leakyrelu'))
            c4.add(VGGLayer(filters[3], 3, norm_layer, 'leakyrelu'))
            c4.add(VGGLayer(filters[3], 3, norm_layer, 'leakyrelu'))
            c4.add(VGGLayer(filters[3], 3, norm_layer, 'leakyrelu'))
            self.ops.append(c4)
            self.mps.append(layers.MaxPool2D(pool_size=2, strides=2, padding='valid'))
        if len(filters) > 4: # 4 x 4 -> 4 x 4     //  8 x 8 -> 4 x 4
            c5 = tf.keras.Sequential()
            c5.add(VGGLayer(filters[4], 3, norm_layer, 'leakyrelu'))
            c5.add(VGGLayer(filters[4], 3, norm_layer, 'leakyrelu'))
            c5.add(VGGLayer(filters[4], 3, norm_layer, 'leakyrelu'))
            c5.add(VGGLayer(filters[4], 3, norm_layer, 'leakyrelu'))
            self.ops.append(c5)
            if hparams.image_size == [96, 128] or hparams.image_size == [128, 128]:
                self.mps.append(layers.MaxPool2D(pool_size=2, strides=2, padding='valid'))
        if len(filters) > 5: #                    //  4 x 4 -> 4 x 4
            assert hparams.image_size == [96, 128] or hparams.image_size == [128, 128]
            c5 = tf.keras.Sequential()
            c5.add(VGGLayer(filters[5], 3, norm_layer, 'leakyrelu'))
            c5.add(VGGLayer(filters[5], 3, norm_layer, 'leakyrelu'))
            c5.add(VGGLayer(filters[5], 3, norm_layer, 'leakyrelu'))
            c5.add(VGGLayer(filters[5], 3, norm_layer, 'leakyrelu'))
            self.ops.append(c5)
        if (len(filters) > 4 and (hparams.image_size == [48, 64] or hparams.image_size == [64, 64])) or \
            (len(filters) > 5 and (hparams.image_size == [96, 128] or hparams.image_size == [128, 128])):
            assert hparams.rnn == 'convlstm'
            if hparams.image_size == [48, 64] or hparams.image_size == [96, 128]:
                self.mps.append(layers.MaxPool2D(pool_size=(3, 4), strides=1, padding='valid'))
            elif hparams.image_size == [64, 64] or hparams.image_size == [128, 128]:
                self.mps.append(layers.MaxPool2D(pool_size=(4, 4), strides=1, padding='valid'))
            else:
                raise NotImplementedError

    def call(self, x, training):
        h, skip = x, []
        for i, (op, mp) in enumerate(zip(self.ops, self.mps)):
            h = op(h, training=training)
            skip.append(h)
            h = mp(h)
        _, _h, _w, _ = h.shape
        if _h == 1:
            h = tf.squeeze(h, axis=1)
        if _w == 1:
            h = tf.squeeze(h, axis=1)
        return h, skip

class VGGDecoder(tf.keras.Model):
    def __init__(self, hparams):
        super(VGGDecoder, self).__init__()
        self.hparams = hparams
        filters = hparams.dec_filters
        norm_layer = hparams.norm_layer
        up_layer = hparams.up_layer

        self.ops = []
        self.ups = []
        filters_n, filters_na = 0, 0
        if hparams.rnn == 'lstm':
            if hparams.image_size == [48, 64] or hparams.image_size == [96, 128]:
                _size=(3, 4)
            elif hparams.image_size == [64, 64] or hparams.image_size == [128, 128]:
                _size=(4, 4)
            self.ups.append(get_upsampling(_size, up_layer))
            upc = tf.keras.Sequential()
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n+1], 3, norm_layer, 'leakyrelu'))
            self.ops.append(upc)
            filters_n += 1
            filters_na += 1
        if len(filters)-filters_na > 4:
            self.ups.append(get_upsampling(2, up_layer))
            upc = tf.keras.Sequential()
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n+1], 3, norm_layer, 'leakyrelu'))
            self.ops.append(upc)
            filters_n += 1
        if len(filters)-filters_na > 3:
            self.ups.append(get_upsampling(2, up_layer))
            upc = tf.keras.Sequential()
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n+1], 3, norm_layer, 'leakyrelu'))
            self.ops.append(upc)
            filters_n += 1
        if len(filters)-filters_na > 2:
            self.ups.append(get_upsampling(2, up_layer))
            upc = tf.keras.Sequential()
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n+1], 3, norm_layer, 'leakyrelu'))
            self.ops.append(upc)
            filters_n += 1
        if len(filters)-filters_na > 1:
            self.ups.append(get_upsampling(2, up_layer))
            upc = tf.keras.Sequential()
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(filters[filters_n+1], 3, norm_layer, 'leakyrelu'))
            self.ops.append(upc)
            filters_n += 1
        if len(filters)-filters_na > 0:
            self.ups.append(get_upsampling(2, up_layer))
            upc = tf.keras.Sequential()
            upc.add(VGGLayer(filters[filters_n], 3, norm_layer, 'leakyrelu'))
            upc.add(VGGLayer(hparams.channels, 3, norm_layer='none', activation='none'))
            upc.add(layers.Activation(tf.nn.sigmoid))
            self.ops.append(upc)
        
    def call(self, vec, skip, training):
        h = vec
        for i, (op, up) in enumerate(zip(self.ops, self.ups)):
            if self.hparams.rnn == 'lstm' and i == 0:
                h = h[:, None, None]
            h = up(h)
            h = op(tf.concat([h, skip[-i-1]], axis=-1), training=training)
        return h

class LSTM(tf.keras.Model):
    def __init__(self, in_dim, n_layers, hparams):    
        super(LSTM, self).__init__()
        self.in_dim = in_dim
        self.n_layers = n_layers
        self.hparams = hparams
        if self.hparams.rnn_norm_h:
            assert self.hparams.rnn == 'convlstm'
            norm_layer_h = hparams.norm_layer
        if self.hparams.rnn_norm_s:
            assert self.hparams.rnn == 'convlstm'
            norm_layer_s = hparams.norm_layer

        if self.hparams.rnn == 'lstm':
            self.lstms = [layers.LSTMCell(hparams.lstm_filter,
                                          kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02),
                                          recurrent_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)
                                          ) for i in range(self.n_layers)]
        else:
            self.lstms = [layers.ConvLSTM2D(hparams.lstm_filter, 3, padding='same',
                                          kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02),
                                          recurrent_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)
                                          ) for i in range(self.n_layers)]
            if self.hparams.rnn_norm_h:
                self.h_norms = [get_norm_layer(norm_layer_h) for i in range(self.n_layers)]
            if self.hparams.rnn_norm_s:
                self.s_norms = [get_norm_layer(norm_layer_s) for i in range(self.n_layers)]

        self._is_build = False

    def init_hidden(self, batch_size):
        hidden = []
        for i in range(self.n_layers):
            if self.hparams.rnn == 'lstm':
                hidden.append(self.lstms[i].get_initial_state(batch_size=batch_size, dtype=tf.float32))
            else:
                if not self._is_build:
                    self.lstms[i].cell.build([batch_size, 1, self.hparams.image_size[0]//np.power(2, len(self.hparams.enc_filters)),
                                              self.hparams.image_size[1]//np.power(2, len(self.hparams.enc_filters)),
                                              self.in_dim if i == 0 else self.hparams.lstm_filter])
                hidden.append(self.lstms[i].get_initial_state(tf.zeros((batch_size, 1,
                                                                        self.hparams.image_size[0]//np.power(2, len(self.hparams.enc_filters)),
                                                                        self.hparams.image_size[1]//np.power(2, len(self.hparams.enc_filters)),
                                                                        self.in_dim if i == 0 else self.hparams.lstm_filter
                                                                        ))))
        self._is_build = True
        return hidden

    def call(self, x, state, training):
        h_in = x
        for i in range(self.n_layers):
            if self.hparams.rnn == 'lstm':
                h_in, state[i] = self.lstms[i](h_in, state[i])
            else:
                h_in, state[i] = self.lstms[i].cell(h_in, state[i])
                if self.hparams.rnn_norm_h:
                    if 'batch' in self.hparams.norm_layer:
                        h_in = self.h_norms[i](h_in, training=training)
                    else:
                        h_in = self.h_norms[i](h_in)
                if self.hparams.rnn_norm_s:
                    if 'batch' in self.norm_layer_s:
                        state = self.s_norms[i](state, training=training)
                    else:
                        state = self.s_norms[i](state)
        return h_in, state

class GaussianLSTM(LSTM):
    def __init__(self, in_dim, n_layers, hparams):  
        super(GaussianLSTM, self).__init__(in_dim, n_layers, hparams)
        if self.hparams.rnn == 'convlstm' and self.hparams.rnn_pooling:
            if self.hparams.image_size == [48, 64] or self.hparams.image_size == [96, 128]:
                size = (3, 4)
            elif self.hparams.image_size == [64, 64] or self.hparams.image_size == [128, 128]:
                size = (4, 4)
            self.mp = layers.MaxPool2D(pool_size=size, strides=1, padding='valid')

        self.mu_net = layers.Dense(hparams.z_dim,
                                   kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)
                                   )
        self.logvar_net = layers.Dense(hparams.z_dim,
                                       kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)
                                       )
    
    def reparameterize(self, mu, logvar):
        sigma = tf.sqrt(tf.exp(logvar))
        eps = tf.compat.v1.random_normal(sigma.shape, dtype=tf.dtypes.float32)        
        return mu + sigma*eps

    def call(self, x, state, training):
        h_in, state = super(GaussianLSTM, self).call(x, state, training)
        if self.hparams.rnn == 'convlstm':
            if not self.hparams.rnn_pooling:
                h_in = tf.reshape(h_in, (-1, self.hparams.image_size[0]//np.power(2, len(self.hparams.enc_filters)) *
                                        self.hparams.image_size[1]//np.power(2, len(self.hparams.enc_filters)) *
                                        self.hparams.lstm_filter))
            else:
                h_in = self.mp(h_in)
                h_in = tf.squeeze(h_in, axis=1)
                h_in = tf.squeeze(h_in, axis=1)
        
        mu = self.mu_net(h_in)
        logvar = self.logvar_net(h_in)
        z = self.reparameterize(mu, logvar)
        return z, mu, logvar, state

class PredModel(BaseModel):
    def __init__(self, hparams_dict=None, **hparams):
        super(PredModel, self).__init__(hparams_dict, **hparams)
        self.encoder = VGGEncoder(self.hparams)
        self.decoder = VGGDecoder(self.hparams)

        self.posterior = GaussianLSTM(in_dim=self.hparams.lstm_filter,
                                      n_layers=self.hparams.posterior_rnn_layers,
                                      hparams=self.hparams)
        if self.hparams.learn_prior:
            self.prior = GaussianLSTM(in_dim=self.hparams.lstm_filter,
                                      n_layers=self.hparams.prior_rnn_layers,
                                      hparams=self.hparams)
        self.frame_predictor = LSTM(in_dim=self.hparams.lstm_filter,
                                    hparams=self.hparams,
                                    n_layers=self.hparams.predictor_rnn_layers)

        def get_action_append_net(n_layers, out_size, h_size=0, out_activation=False):
            if n_layers == 1:
                return layers.Dense(out_size, kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02))
            if n_layers > 1:
                assert h_size > 0
            op = tf.keras.Sequential()
            for i in range(n_layers):
                if i < n_layers - 1:
                    assert h_size > 0
                    op.add(layers.Dense(h_size, kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)))
                    op.add(layers.LeakyReLU(0.2))
                else:
                    op.add(layers.Dense(out_size, kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)))
                    if out_activation:
                        op.add(layers.LeakyReLU(0.2))
            return op
        
        self.enc_action_append = None

        def _get_action_append_net_warp():
            if self.hparams.rnn == 'convlstm' and self.hparams.action_append_mode == 'flat':
                return get_action_append_net(n_layers=self.hparams.ac_mpl_layers,
                                             out_size=self.hparams.image_size[0]//np.power(2, len(self.hparams.enc_filters)) *
                                             self.hparams.image_size[1]//np.power(2, len(self.hparams.enc_filters)) *
                                             self.hparams.action_append_channels)
            else:
                return get_action_append_net(n_layers=self.hparams.ac_mpl_layers, out_size=self.hparams.action_append_channels)

        def get_action_append_net_warp():
            if self.hparams.action_append_enc_mode == 'same':
                if self.enc_action_append is None:
                    self.enc_action_append = _get_action_append_net_warp()
                    return self.enc_action_append
            elif self.hparams.action_append_enc_mode == 'diff':
                return _get_action_append_net_warp()
            else:
                raise NotImplementedError

        if self.hparams.action_conditional or self.hparams.use_state:
            assert self.hparams.action_append_channels > 0
            if self.hparams.learn_prior:
                self.enc_action_append_prior = get_action_append_net_warp()
            if self.hparams.action_append_enc_post:
                self.enc_action_append_post = get_action_append_net_warp()
            if self.hparams.action_append_enc_pred:
                self.enc_action_append_pred = get_action_append_net_warp()
        
        def get_enc_rnn_embed():
            if self.hparams.rnn == 'convlstm':
                enc_rnn_embed = tf.keras.Sequential()
                enc_rnn_embed.add(layers.Conv2D(self.hparams.lstm_filter, 1, padding='same',
                                                kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)
                                                ))
                enc_rnn_embed.add(get_norm_layer(self.hparams.norm_layer))
                enc_rnn_embed.add(layers.LeakyReLU(0.2))
            elif self.hparams.rnn == 'lstm':
                enc_rnn_embed = tf.keras.Sequential()
                enc_rnn_embed.add(layers.Dense(self.hparams.lstm_filter,
                                               kernel_initializer=tf.keras.initializers.RandomNormal(mean=0., stddev=0.02)
                                               ))
                enc_rnn_embed.add(layers.LeakyReLU(0.2))
            else:
                raise NotImplementedError
            return enc_rnn_embed

        self.enc_rnn_embed_pred = get_enc_rnn_embed()
        if self.hparams.action_conditional or self.hparams.use_state:
            if self.hparams.learn_prior:
                self.enc_rnn_embed_prior = get_enc_rnn_embed()
                if self.hparams.action_append_enc_post:
                    self.enc_rnn_embed_post = get_enc_rnn_embed()
      
    def get_default_hparams(self):
        hparams = dict(
            image_size=[64, 64],
            channels=3,
            action_conditional=False,
            use_state=False,
            action_append_mode='flat',
            action_append_channels=0,
            action_append_enc_post=False,
            action_append_enc_pred=False,
            action_append_enc_mode='same',
            context_frames=2,
            learn_prior=True,
            z_dim=64,
            predictor_rnn_layers=2,
            posterior_rnn_layers=1,
            prior_rnn_layers=1,
            ac_mpl_layers=1,
            last_frame_skip=False,
            enc_filters=[64, 128, 256, 512],
            dec_filters=[512, 256, 128, 64],
            lstm_filter=512,            
            norm_layer='batch',
            up_layer='upsampling',
            beta=1e-04,
            loss='l1',
            schedule_sampling='inverse_sigmoid',
            schedule_sampling_k=1800.0,
            schedule_sampling_steps=[0, 100000],
            rnn='convlstm',
            rnn_norm_h=False,
            rnn_norm_s=False,
            rnn_pooling=False,
            kdl_mode='mean'
        )
        parent_params = super(PredModel, self).get_default_hparams()
        for k in hparams.keys():
            parent_params.add_hparam(k, hparams[k])
        return parent_params
 
    def gen_schedule_sampling_gt(self, T, B):
        global_step = tf.compat.v1.train.get_or_create_global_step()
        ground_truth_sampling_shape = [T - 1 - self.hparams.context_frames, B]
        if self.hparams.schedule_sampling == 'none':
            ground_truth_sampling = tf.constant(False, dtype=tf.bool, shape=ground_truth_sampling_shape)
        elif self.hparams.schedule_sampling in ('inverse_sigmoid', 'linear'):
            if self.hparams.schedule_sampling == 'inverse_sigmoid':
                k = self.hparams.schedule_sampling_k
                start_step = self.hparams.schedule_sampling_steps[0]
                iter_num = tf.compat.v1.to_float(global_step)
                prob = (k / (k + tf.exp((iter_num - start_step) / k)))
                prob = tf.cond(tf.less(iter_num, start_step), lambda: 1.0, lambda: prob)
            elif self.hparams.schedule_sampling == 'linear':
                start_step, end_step = self.hparams.schedule_sampling_steps
                step = tf.compat.v1.clip_by_value(global_step, start_step, end_step)
                prob = 1.0 - tf.compat.v1.to_float(step - start_step) / tf.compat.v1.to_float(end_step - start_step)
            log_probs = tf.compat.v1.log([1 - prob, prob])
            ground_truth_sampling = tf.compat.v1.multinomial([log_probs] * B, ground_truth_sampling_shape[0])
            ground_truth_sampling = tf.cast(tf.transpose(ground_truth_sampling, [1, 0]), dtype=tf.bool)
            # Ensure that eventually, the model is deterministically
            # autoregressive (as opposed to autoregressive with very high probability).
            ground_truth_sampling = tf.cond(tf.less(prob, 0.001),
                                            lambda: tf.constant(False, dtype=tf.bool, shape=ground_truth_sampling_shape),
                                            lambda: ground_truth_sampling)
        else:
            raise NotImplementedError
        return ground_truth_sampling
        
    def schedule_sampling(self, schedule_sampling_gt, i, x_i_1, x_pred, p_i_1=None, p_pred=None):
        x_in = tf.compat.v1.where(schedule_sampling_gt[i], x_i_1, x_pred)  # schedule sampling (if any)
        return x_in

    def _append_actions(self, _enc_action_append, action_state):
        if self.hparams.rnn == 'convlstm':
            if self.hparams.action_append_mode == 'flat':
                return tf.reshape(_enc_action_append(action_state),
                                  (-1, self.hparams.image_size[0]//np.power(2, len(self.hparams.enc_filters)),
                                   self.hparams.image_size[1]//np.power(2, len(self.hparams.enc_filters)), self.hparams.action_append_channels))
            else:
                return tf.tile(_enc_action_append(action_state)[:, None, None],
                               (1, self.hparams.image_size[0]//np.power(2, len(self.hparams.enc_filters)),
                                self.hparams.image_size[1]//np.power(2, len(self.hparams.enc_filters)), 1))
        else:
            return _enc_action_append(action_state)

    def model(self, x, actions=None, states=None, frame_predictor_hidden=None, posterior_hidden=None, prior_hidden=None, training=False):
        outputs = OrderedDict()
        T, B = x.shape[:2]
        if training:
            schedule_sampling_gt = self.gen_schedule_sampling_gt(T, B)
            outputs['gt_sampling_mean'] = tf.reduce_mean(tf.compat.v1.to_float(schedule_sampling_gt))
            h_target = self.encoder(x[0], training)[0]
            _, _, _, posterior_hidden = self.posterior(h_target, posterior_hidden, training)
        for i in range(1, T):
            if i <= self.hparams.context_frames:
                x_in = x[i-1]
            elif training:
                x_in = self.schedule_sampling(schedule_sampling_gt, i-self.hparams.context_frames-1, x[i-1], x_pred)
            else:
                x_in = x_pred
            h = self.encoder(x_in, training)
            h_target = self.encoder(x[i], training)[0]
            if self.hparams.last_frame_skip or i <= self.hparams.context_frames:	
                h, skip = h
            else:
                h = h[0]
            
            if self.hparams.action_conditional or self.hparams.use_state:
                action_state = None
                if self.hparams.action_conditional:
                    assert actions is not None
                    action_state = actions[i-1]
                if self.hparams.use_state:
                    assert states is not None
                    if action_state is None:
                        action_state = states[i-1]
                    else:
                        action_state = tf.concat(
                            [action_state, states[i-1]], axis=-1)
                if self.hparams.learn_prior:
                    action_append_prior = self._append_actions(self.enc_action_append_prior, action_state)
                if self.hparams.action_append_enc_post:
                    action_append_post = self._append_actions(self.enc_action_append_post, action_state)
                    h_target = self.enc_rnn_embed_post(tf.concat((h_target, action_append_post), -1), training=training)
                if self.hparams.action_append_enc_pred:
                    action_append_pred = self._append_actions(self.enc_action_append_pred, action_state)
           
            z_t, mu, logvar, posterior_hidden = self.posterior(h_target, posterior_hidden, training)
            outputs['zs'] = outputs.get('zs', []) + [z_t]
            outputs['zs_mu'] = outputs.get('zs_mu', []) + [mu]
            outputs['zs_log_sigma_sq'] = outputs.get('zs_log_sigma_sq', []) + [logvar]
            
            if prior_hidden is not None:
                if self.hparams.action_conditional or self.hparams.use_state:
                    h_append = self.enc_rnn_embed_prior(tf.concat((h, action_append_prior), -1), training=training)
                else:
                    h_append = h
                z_t_p, mu_p, logvar_p, prior_hidden = self.prior(h_append, prior_hidden, training)
                outputs['zs_prior'] = outputs.get('zs_prior', []) + [z_t_p]
                outputs['zs_mu_prior'] = outputs.get('zs_mu_prior', []) + [mu_p]
                outputs['zs_log_sigma_sq_prior'] = outputs.get('zs_log_sigma_sq_prior', []) + [logvar_p]
            else:
                z_t_p = tf.compat.v1.random_normal((B, self.hparams.z_dim), dtype=tf.dtypes.float32)

            if (not training) and (i >= self.hparams.context_frames):
                z_t = z_t_p

            if self.hparams.rnn == 'convlstm':
                h_pred = tf.concat([h, tf.tile(z_t[:, None, None], (1, self.hparams.image_size[0]//np.power(2, len(self.hparams.enc_filters)),
                                                                    self.hparams.image_size[1]//np.power(2, len(self.hparams.enc_filters)), 1))],
                                   axis=-1)
            else:
                h_pred = tf.concat([h, z_t], axis=-1)
            
            if self.hparams.action_conditional or self.hparams.use_state:
                if self.hparams.action_append_enc_pred:
                    h_pred = self.enc_rnn_embed_pred(tf.concat((h_pred, action_append_pred), -1), training=training)
                else:
                    h_pred = self.enc_rnn_embed_pred(h_pred, training=training)

            h_pred, frame_predictor_hidden = self.frame_predictor(h_pred,
                                                                  frame_predictor_hidden,
                                                                  training)
            x_pred = self.decoder(h_pred, skip, training)
            outputs['gen_rec'] = outputs.get('gen_rec', []) + [x_pred]

        outputs = OrderedDict([(key, tf.concat([x[None] for x in output], 0) if isinstance(output, list) else output) for key, output in outputs.items()])
        outputs = OrderedDict([(key, tf.transpose(output, [1, 0] + list(range(2, output.shape.ndims))) if output.shape.ndims > 1 else output) for key, output in outputs.items()])
        return outputs
    
    def call(self, inputs_, training):
        inputs = self.preprocess_input(inputs_)
        x = inputs['images']
        x = tf.transpose(x, [1, 0] + list(range(2, x.shape.ndims)))
        if 'actions' in inputs:
            actions = inputs['actions']
            actions = tf.transpose(actions, [1, 0] + list(range(2, actions.shape.ndims)))
        else:
            actions = None
        if 'states' in inputs:
            states = inputs['states']
            states = tf.transpose(states, [1, 0] + list(range(2, states.shape.ndims)))
        else:
            actions = None
            
        # T, B, H, W, C = x.shape
        frame_predictor_hidden = self.frame_predictor.init_hidden(self.batch_size)
        posterior_hidden = self.posterior.init_hidden(self.batch_size)
        if self.hparams.learn_prior:
            prior_hidden = self.prior.init_hidden(self.batch_size)
        else:
            prior_hidden = None

        outputs = self.model(x, actions=actions, states=states, frame_predictor_hidden=frame_predictor_hidden,
                             posterior_hidden=posterior_hidden, prior_hidden=prior_hidden, training=training)
        return self.optimize_step(inputs, outputs)


    def evaluate_sample(self, inputs_):
        inputs = self.preprocess_input(inputs_)
        x = inputs['images']
        if 'actions' in inputs:
            actions = inputs['actions']
            actions = tf.transpose(actions, [1, 0] + list(range(2, actions.shape.ndims)))
        else:
            actions = None
        if 'states' in inputs:
            states = inputs['states']
            states = tf.transpose(states, [1, 0] + list(range(2, states.shape.ndims)))
        else:
            actions = None

        outputs_dict = OrderedDict()
        outputs_dict['scale'] = OrderedDict()
        outputs_dict['tensor'] = OrderedDict()

        n_samples = 100
        B, T, H, W, C = x.shape
        x = tf.concat([x[:, :self.hparams.context_frames], tf.zeros_like(x[:, self.hparams.context_frames:], dtype=tf.float32)], axis=1)
        i = tf.constant(0)
        n_metrics = len(self.metric_fns)
        outputs_scale = tf.zeros_like(tf.tile(x[:, :1, :1, :1, 0], (1, n_metrics, n_samples, T-self.hparams.context_frames)), dtype=tf.float32)
        outputs_tensor = tf.zeros_like(tf.tile(x[:, :1][:, None, None], (1, n_metrics, 4, T-self.hparams.context_frames, 1, 1, 1)), dtype=tf.float32)
        outputs_id3 = tf.zeros_like(tf.tile(x[:, :1, :1, 0, 0], (1, n_samples, 400)), dtype=tf.float32)

        x = tf.transpose(x, [1, 0] + list(range(2, x.shape.ndims)))
        def one_sample(i, outputs_scale, outputs_tensor, outputs_id3):            
            frame_predictor_hidden = self.frame_predictor.init_hidden(B)
            posterior_hidden = self.posterior.init_hidden(B)
            if self.hparams.learn_prior:
                prior_hidden = self.prior.init_hidden(B)
            else:
                prior_hidden = None
            outputs = self.model(x, actions, states, frame_predictor_hidden, posterior_hidden, prior_hidden)
            
            gt = inputs['images'][:, self.hparams.context_frames:]
            rec = outputs['gen_rec'][:, self.hparams.context_frames-1:]

            outputs_scale, outputs_tensor, outputs_id3 = self.update_outputs(
                i, T, gt, rec, outputs_scale, outputs_tensor, outputs_id3)

            # outputs_id3 = tf.reshape(outputs_id3, (-1, 100, 400))
            return [i+1, outputs_scale, outputs_tensor, outputs_id3]

        _, outputs_scale, outputs_tensor, outputs_id3 = tf.while_loop(lambda i, os, ot, oi: i < n_samples, one_sample,
                                                         loop_vars=[i, outputs_scale, outputs_tensor, outputs_id3],
                                                         shape_invariants=[i.get_shape(),
                                                                           tf.TensorShape([None, n_metrics, n_samples, T-self.hparams.context_frames]),
                                                                           tf.TensorShape([None, n_metrics, 4, T-self.hparams.context_frames, H, W, C]),
                                                                           tf.TensorShape([None, n_samples, 400])])
        
        outputs_tensor = tf.concat([outputs_tensor[:,:,:-2], outputs_tensor[:,:,-1:], outputs_tensor[:,:,-2:-1]/100], axis=2)
        for i, (key, _) in enumerate(self.metric_fns):
            outputs_dict['scale'][key] = outputs_scale[:, i]
            outputs_dict['tensor'][key] = tf.concat([inputs['images'][:, None, ],
                                                    tf.concat([tf.tile(inputs['images'][:, None, :self.hparams.context_frames], (1, 4, 1, 1, 1, 1)),
                                                                outputs_tensor[:, i]], axis=2)], axis=1)
            outputs_dict['tensor'][key] = tf.transpose(outputs_dict['tensor'][key], (0, 2, 1, 3, 4, 5))
            B, T, R, H, W, C = outputs_dict['tensor'][key].shape
            outputs_dict['tensor'][key] = tf.reshape(outputs_dict['tensor'][key], (B, T, R*H, W, C))
        
        # outputs_dict['scale']['id3'] = outputs_id3

        return outputs_dict


    @property
    def d_vars(self):
        vars_disc = []
        return vars_disc
    
    @property
    def g_vars(self):
        return [v for v in self.trainable_variables if v.name not in [v.name for v in self.d_vars]]
    