import itertools
import collections
import tensorflow as tf
from tensor2tensor.utils.hparam import HParams
from collections import OrderedDict
from vpgo.utils.fvd import FVD
from vpgo.utils.lpips import LPIPS

def kl_loss(mu, log_sigma_sq, mu2=None, log_sigma2_sq=None, mode=None):
    if mu2 is None and log_sigma2_sq is None:
        sigma_sq = tf.exp(log_sigma_sq)
        kld = 1 + log_sigma_sq - tf.square(mu) - sigma_sq
        kld = kld * (-0.5)
    else:
        mu1 = mu
        log_sigma1_sq = log_sigma_sq
        kld = (log_sigma2_sq - log_sigma1_sq) / 2 + (tf.exp(log_sigma1_sq) + tf.square(mu1 - mu2)) / (2 * tf.exp(log_sigma2_sq)) - 1 / 2
    
    if mode == 'sum':
        kld = tf.reduce_sum(kld, axis=-1)
    elif mode == 'mean':
        kld = tf.reduce_mean(kld, axis=-1)
    else:
        raise NotImplementedError

    kld = tf.reduce_mean(kld, axis=-1)
    return kld
        

class BaseModel(tf.keras.Model):
    def __init__(self, hparams_dict=None, **hparams):
        super(BaseModel, self).__init__()
        self.lpips_fn = LPIPS()
        self.fvd_fn = FVD()
        self.batch_size = hparams.pop('batch_size', 0)
        assert self.batch_size != 0
        self.n_gpus = hparams.pop('n_gpus', 0)
        assert self.n_gpus != 0
        self.global_batch_size = self.batch_size*self.n_gpus        
        self.hparams = self.parse_hparams(hparams_dict, hparams)
              
    def parse_hparams(self, hparams_dict, hparams):
        deprecated_hparams_keys = [
        ]
        for deprecated_hparams_key in deprecated_hparams_keys:
            hparams_dict.pop(deprecated_hparams_key, None)

        parsed_hparams = self.get_default_hparams().override_from_dict(hparams_dict or {})
        if hparams:
            if not isinstance(hparams, (list, tuple)):
                hparams = [hparams]
            for hparam in hparams:
                parsed_hparams.parse(hparam)
        return parsed_hparams

    def get_default_hparams(self):
        hparams = dict(
        )
        return HParams(**hparams)

    def preprocess_input(self, inputs_):
        inputs = OrderedDict()        
        if not isinstance(inputs_, tf.Tensor):
            assert 'images' in inputs_
            inputs['images'] = inputs_['images']
            if 'actions' in inputs_:
                inputs['actions'] = inputs_['actions']
            if 'states' in inputs_:
                inputs['states'] = inputs_['states']
            else:
                raise NotImplementedError
        else:
            inputs['images'] = inputs_
        return inputs

    def optimize_step(self, inputs, outputs):
        outputs_dict = OrderedDict()
        if self.hparams.loss == 'l1':
            loss = tf.nn.compute_average_loss(tf.keras.losses.MAE(tf.reshape(inputs['images'][:, 1:], (inputs['images'].shape[0], -1)),
                                                                  tf.reshape(outputs['gen_rec'], (outputs['gen_rec'].shape[0], -1))),
                                              global_batch_size=self.global_batch_size)
            outputs_dict['loss_gen_l1'] = loss
        elif self.hparams.loss == 'l2':
            loss = tf.nn.compute_average_loss(tf.keras.losses.MSE(tf.reshape(inputs['images'][:, 1:], (inputs['images'].shape[0], -1)),
                                                                  tf.reshape(outputs['gen_rec'], (outputs['gen_rec'].shape[0], -1))),
                                              global_batch_size=self.global_batch_size)
            outputs_dict['loss_gen_l2'] = loss
        else:
            raise NotImplementedError

        if self.hparams.learn_prior:
            outputs_dict['loss_gen_kld'] = tf.nn.compute_average_loss(kl_loss(outputs['zs_mu'], outputs['zs_log_sigma_sq'],
                                                                            outputs['zs_mu_prior'], outputs['zs_log_sigma_sq_prior'], self.hparams.kdl_mode),
                                                                    global_batch_size=self.global_batch_size)
        else:
            outputs_dict['loss_gen_kld'] = tf.nn.compute_average_loss(kl_loss(outputs['zs_mu'], outputs['zs_log_sigma_sq'], mode=self.hparams.kdl_mode),
                                                                    global_batch_size=self.global_batch_size)

        outputs_dict['loss_gen'] = loss + self.hparams.beta*outputs_dict['loss_gen_kld']

        outputs_dict = collections.OrderedDict(itertools.chain(*[output.items() for output in [outputs, outputs_dict]]))
        outputs_dict['images'] = tf.concat([inputs['images'], tf.concat([inputs['images'][:, :self.hparams.context_frames],
                                                                         outputs['gen_rec'][:, self.hparams.context_frames-1:]], axis=1)], axis=2)
        return outputs_dict

    def update_outputs(self, i, T, gt, rec, outputs_scale, outputs_tensor, outputs_id3):
        n_metrics = len(self.metric_fns)
        metrics = []
        for (key, fn) in self.metric_fns:
            if key in ['psnr', 'ssim']:
                metrics.append(fn(gt, rec, max_val=1.0)[:, None])
            elif key == 'lpips':
                metrics.append(fn(gt, rec)[:, None])
            else:
                raise NotImplementedError
        metrics = tf.concat(metrics, axis=1)
        outputs_scale = tf.concat([outputs_scale[:, :, :i], metrics[:, :, None], outputs_scale[:, :, i+1:]], axis=2)
        outputs_scale = tf.reshape(outputs_scale, (-1, n_metrics, 100, T-self.hparams.context_frames))

        if self.hparams.image_size == [48, 64]:
            id3 = self.fvd_fn.get_i3d(tf.concat([tf.zeros_like(rec[:, :, :8, :, :], dtype=tf.uint8),
                                        tf.cast(rec*255, dtype=tf.uint8),
                                        tf.zeros_like(rec[:, :, :8, :, :], dtype=tf.uint8)], axis=2))
        elif self.hparams.image_size == [64, 64]:
            id3 = self.fvd_fn.get_i3d(tf.cast(rec*255, dtype=tf.uint8),
                                    tf.cast(rec*255, dtype=tf.uint8))
        else:
            raise NotImplementedError
        # outputs_id3 = tf.concat([outputs_id3[:, :i], id3[:, None], outputs_id3[:, i+1:]], axis=1)           
        
        if i == 0:
            outputs_tensor = tf.tile(rec[:, None, None], (1, n_metrics, 4, 1, 1, 1, 1))
        else:
            outputs_tensor = tf.concat([outputs_tensor[:, :, :-2],
                                        (outputs_tensor[:, :, -2] + tf.tile(rec[:, None], (1, n_metrics, 1, 1, 1, 1)))[:, :, None],
                                        tf.tile(rec[:, None], (1, n_metrics, 1, 1, 1, 1))[:, :, None]], axis=2)
            
            outputs_tensor = tf.concat([
                        tf.where((tf.reduce_mean(metrics, axis=-1) > tf.reduce_max(tf.reduce_mean(outputs_scale[:, :, :i], axis=-1), axis=-1))[..., None, None, None, None],
                                tf.tile(rec[:, None], (1, n_metrics, 1, 1, 1, 1)), outputs_tensor[:, :, 0])[:, :, None],
                        tf.where((tf.reduce_mean(metrics, axis=-1) < tf.reduce_min(tf.reduce_mean(outputs_scale[:, :, :i], axis=-1), axis=-1))[..., None, None, None, None],
                                tf.tile(rec[:, None], (1, n_metrics, 1, 1, 1, 1)), outputs_tensor[:, :, 1])[:, :, None],
                        outputs_tensor[:, :, 2:]], axis=2)
        
        return outputs_scale, outputs_tensor, outputs_id3

    @property
    def metric_fns(self):
        return [('psnr', tf.image.psnr),
                ('ssim', tf.image.ssim),
                ('lpips', self.lpips_fn)]
