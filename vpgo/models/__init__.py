from vpgo.models.pred_model import PredModel

def get_model_class(model):
    model_mappings = {
        'vpgo': 'PredModel',
    }
    model_class = model_mappings.get(model, model)
    model_class = globals().get(model_class)
    return model_class
