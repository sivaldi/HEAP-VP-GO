
import yaml
import re
import imageio
import numpy as np
import tensorflow as tf

def load_hparams(yaml_path):
    with open(yaml_path) as f:
        metadata = yaml.safe_load(f)

    hparams_dict = {}
    hparams_dict['dataset_params'] = metadata.pop('DATASET_HPARAMS', dict())
    if hparams_dict['dataset_params'] is None:
        hparams_dict['dataset_params'] = dict()
    hparams_dict['model_params'] = metadata.pop('MODEL_HPARAMS', dict())
    if hparams_dict['model_params'] is None:
        hparams_dict['model_params'] = dict()
    
    for k, v in metadata.items():
        hparams_dict[k] = v
        
    hparams_dict['yaml_file'] = '_'.join(re.split('/', yaml_path)[1:])
        
    return hparams_dict


def add_border(tensor, img_size, c_frames, pad):
    images = np.array(tensor)
    H, W = img_size
    B, T, _, _, C = images.shape
    images = np.reshape(images, (B, T, -1, H, W, C))
    images[:, :, :1, :, :pad] = np.tile(np.array([0, 0.7, 0])[None, None], (H, pad, 1))
    images[:, :, :1, :, W-pad:] = np.tile(np.array([0, 0.7, 0])[None, None], (H, pad, 1))
    images[:, :, :1, :pad] = np.tile(np.array([0, 0.7, 0])[None, None], (pad, W, 1))
    images[:, :, :1, H-pad:] = np.tile(np.array([0, 0.7, 0])[None, None], (pad, W, 1))

    images[:, :c_frames, 1:, :, :pad] = np.tile(np.array([0, 0.7, 0])[None, None], (H, pad, 1))
    images[:, :c_frames, 1:, :, W-pad:] = np.tile(np.array([0, 0.7, 0])[None, None], (H, pad, 1))
    images[:, :c_frames, 1:, :pad] = np.tile(np.array([0, 0.7, 0])[None, None], (pad, W, 1))
    images[:, :c_frames, 1:, H-pad:] = np.tile(np.array([0, 0.7, 0])[None, None], (pad, W, 1))

    images[:, c_frames:, 1:, :, :pad] = np.tile(np.array([0.7, 0, 0])[None, None], (H, pad, 1))
    images[:, c_frames:, 1:, :, W-pad:] = np.tile(np.array([0.7, 0, 0])[None, None], (H, pad, 1))
    images[:, c_frames:, 1:, :pad] = np.tile(np.array([0.7, 0, 0])[None, None], (pad, W, 1))
    images[:, c_frames:, 1:, H-pad:] = np.tile(np.array([0.7, 0, 0])[None, None], (pad, W, 1))
    images = np.reshape(images, (B, T, -1, W, C))
    return images

def to_images(tensor, img_size, c_frames, pad=1):
    # tensor: B x T x R*H x W x C
    images = add_border(tensor, img_size, c_frames, pad)
    images = np.transpose(images, (0, 2, 1, 3, 4))
    B, H, T, W, C = images.shape
    return tf.reshape(images, (B, H, T*W, C))

def to_gifs(tensor, img_size, c_frames, pad=1):
    # tensor: B x T x R*H x W x C
    images = add_border(tensor, img_size, c_frames, pad)
    v = (images*255).astype(np.uint8)
    B, T, H, W, C = v.shape
    v = np.transpose(v, (1, 2, 0, 3, 4))
    v = np.reshape(v, (T, H, B*W, C))
    encoded_image_string = encode_gif(v)
    image_summ = tf.compat.v1.Summary.Image()
    image_summ.height = H*np.max((1, B//8))
    image_summ.width = W*np.min((B, 8))
    image_summ.colorspace = C
    image_summ.encoded_image_string = encoded_image_string
    return image_summ

def save_gifs(path, images):
    images = tf.reshape(images, (16, 96, -1, 64, 3))
    images = tf.transpose(images, (2, 1, 0, 3, 4))
    T = images.shape[0]
    images = tf.reshape(images, (T, 96, -1, 3))
    imageio.mimwrite(path+'.gif', images, duration=0.2)
        

def encode_gif(images, fps=5):
    '''
    Encodes numpy images into gif string.
    Args:
        images: A 5-D `uint8` `np.array` (or a list of 4-D images) of shape
            `[batch_size, time, height, width, channels]` where `channels` is 1 or 3.
        fps: frames per second of the animation
    Returns:
        The encoded gif string.
    Raises:
        IOError: If the ffmpeg command returns an error.
    '''
    from subprocess import Popen, PIPE
    h, w, c = images[0].shape
    cmd = [
        'ffmpeg', '-y',
        '-f', 'rawvideo',
        '-vcodec', 'rawvideo',
        '-r', '%.02f' % fps,
        '-s', '%dx%d' % (w, h),
        '-pix_fmt', {1: 'gray', 3: 'rgb24'}[c],
        '-i', '-',
        '-filter_complex',
        '[0:v]split[x][z];[z]palettegen[y];[x]fifo[x];[x][y]paletteuse',
        '-r', '%.02f' % fps,
        '-f', 'gif',
        '-']
    proc = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
    for image in images:
        proc.stdin.write(image.tobytes())
    out, err = proc.communicate()
    if proc.returncode:
        err = '\n'.join([' '.join(cmd), err.decode('utf8')])
        raise IOError(err)
    del proc
    return out