import os
import tensorflow as tf

class LPIPS:
    def __init__(self, model='net-lin', net='alex', version=0.1):
        default_graph = tf.compat.v1.get_default_graph()
        producer_version = default_graph.graph_def_versions.producer

        cache_dir = 'lpips'
        os.makedirs(cache_dir, exist_ok=True)
        
        pb_fnames = [
            '%s_%s_v%s_%d.pb' % (model, net, version, producer_version),
            '%s_%s_v%s.pb' % (model, net, version),
        ]
        for pb_fname in pb_fnames:
            if os.path.isfile(os.path.join(cache_dir, pb_fname)):
                break

        with open(os.path.join(cache_dir, pb_fname), 'rb') as f:
            graph_def = tf.compat.v1.GraphDef()
            graph_def.ParseFromString(f.read())

        def _imports_graph_def():
            tf.compat.v1.import_graph_def(graph_def, name="")
        wrapped_import = tf.compat.v1.wrap_function(_imports_graph_def, [])
        import_graph = wrapped_import.graph

        inputs = ['0:0', '1:0']
        outputs = ['Reshape_10:0']
        self.frozen_func = wrapped_import.prune(
            tf.nest.map_structure(import_graph.as_graph_element, inputs),
            tf.nest.map_structure(import_graph.as_graph_element, outputs))

    def __call__(self, input0, input1):
        batch_shape = tf.shape(input0)[:-3]
        input0 = tf.reshape(input0, tf.concat([[-1], tf.shape(input0)[-3:]], axis=0))
        input1 = tf.reshape(input1, tf.concat([[-1], tf.shape(input1)[-3:]], axis=0))
        # NHWC to NCHW
        input0 = tf.transpose(input0, [0, 3, 1, 2])
        input1 = tf.transpose(input1, [0, 3, 1, 2])
        # normalize to [-1, 1]
        input0 = input0 * 2.0 - 1.0
        input1 = input1 * 2.0 - 1.0

        distance = self.frozen_func(input0, input1)[0]

        if distance.shape.ndims == 4:
            distance = tf.squeeze(distance, axis=[-3, -2, -1])
        # reshape the leading dimensions
        distance = tf.reshape(distance, batch_shape)
        return -distance


if __name__ == '__main__':
    physical_devices = tf.config.list_physical_devices('GPU')
    for device in physical_devices:
        tf.config.experimental.set_memory_growth(device, True)
    lpips_fn = LPIPS()
    NUMBER_OF_VIDEOS = 3
    VIDEO_LENGTH = 15

    first_set_of_videos = tf.zeros([NUMBER_OF_VIDEOS, VIDEO_LENGTH, 48, 64, 3])/255.0
    second_set_of_videos = (tf.ones([NUMBER_OF_VIDEOS, VIDEO_LENGTH, 48, 64, 3]) * 255)/255.0
    result = lpips_fn(first_set_of_videos, second_set_of_videos)

    print(result)