import os
import copy
import tensorflow as tf    
from collections import OrderedDict
from vpgo.datasets.robonet.robonet import RoboNetDataset
from vpgo.datasets.robonet.util.metadata_helper import load_metadata
from vpgo.datasets.panda_grasp import PandaGrasp

def get_dataset(dataset, batch_size, hparams=dict()):
    if dataset == 'RoboNet':
        data_dir = hparams.pop('data_dir', '')
        database = load_metadata(data_dir)    
        robot_names = database['robot'].frame.unique().tolist()
        robots = hparams.pop('robots', [])
        assert len(robots) > 1
        assert set(robots) <= set(robot_names)
        adim = hparams.pop('adim', 0)

        databases = OrderedDict()

        for name in robot_names:
            databases[name] = database[database['robot'] == name]
            if adim > 0:
                databases[name] = databases[name][databases[name]['adim'] == adim]
                print('{:<7s} --> {:<6d}/{:<6d}'.format(name, len(databases[name]), len(database[database['robot'] == name])))
            else:
                print('{:<7s} --> {:<6d}/{:<6d}'.format(name, len(database[database['robot'] == name]), len(database[database['robot'] == name])))

        databases = [databases[robot] for robot in robots]
        for d in databases:
            assert len(d) > 0

        loader = RoboNetDataset(batch_size=batch_size,
                                dataset_files_or_metadata=databases,
                                hparams=hparams)
        return loader

    elif dataset == 'PandaGrasp':
        data_dir = hparams.pop('data_dir', '')
        dataset_params = {
            'load_T': hparams.pop('load_T', 12),
            'use_random_train_seed': True,
        }
        metadata = ['metadata.pkl']

        train_data_loader = PandaGrasp(batch_size=batch_size, data_dir=data_dir,
                                       metadata=metadata,
                                       hparams=copy.deepcopy(dataset_params))
        train_data_loader.set_mode('train')
        val_data_loader = PandaGrasp(batch_size=batch_size, data_dir=data_dir,
                                     metadata=metadata,
                                     hparams=copy.deepcopy(dataset_params))
        val_data_loader.set_mode('val')
        dataset_params_test = copy.deepcopy(dataset_params)
        dataset_params_test['load_T'] = 25
        test_data_loader = PandaGrasp(batch_size=batch_size, data_dir=data_dir,
                                      metadata=metadata,
                                      hparams=copy.deepcopy(dataset_params_test))
        test_data_loader.set_mode('test')

        return [train_data_loader, val_data_loader, test_data_loader]
        
    else:
        raise NotImplementedError
    


