import re
import os
import glob
import csv
import copy
import random
import imageio
import numpy as np
import multiprocessing
from collections import OrderedDict
from tensor2tensor.utils.hparam import HParams
from vpgo.datasets.robonet.util.metadata_helper import load_metadata, MetaDataContainer
from vpgo.datasets.robonet.util.hdf5_loader import load_data, default_loader_hparams
from vpgo.datasets.robonet.util.dataset_utils import color_augment, split_train_val_test

def _load_data(inputs): 
    f_name, file_metadata, hparams, rng, color_augmentation, is_float = inputs    
    outputs = load_data(f_name, file_metadata, hparams, rng)
    if hparams.load_annotations:
        images, actions, states, annotations = outputs
    else:
        images, actions, states = outputs

    if is_float:
        images = (images / 255.0).astype(np.float32)
        if color_augmentation:
            images = color_augment(images, color_augmentation)
    images = np.squeeze(images)

    if hparams.load_annotations:
        return images, actions, states, annotations
    else:
        return images, actions, states

class RoboNetDataset(object):
    def __init__(self, batch_size, dataset_files_or_metadata, hparams=dict()):
        source_probs = hparams.pop('source_selection_probabilities', None)
        assert isinstance(batch_size, int), "batch_size must be an integer"
        self._batch_size = batch_size

        if isinstance(dataset_files_or_metadata, str):
            self._metadata = [load_metadata(dataset_files_or_metadata)]
        elif isinstance(dataset_files_or_metadata, MetaDataContainer):
            self._metadata = [dataset_files_or_metadata]
        elif isinstance(dataset_files_or_metadata, (list, tuple)):
            self._metadata = []
            for d in dataset_files_or_metadata:
                assert isinstance(d, (str, MetaDataContainer)), "potential dataset must be folder containing files or meta-data instance"
                if isinstance(d, str):
                    self._metadata.append(load_metadata(d))
                else:
                    self._metadata.append(d)

        # initialize hparams and store metadata_frame
        self._hparams = self._get_default_hparams().override_from_dict(hparams)
        self._hparams.source_probs = source_probs
        # initialize dataset
        self._init_rng()
        self._init_dataset()

    def _init_dataset(self):
        if self._hparams.load_random_cam and self._hparams.same_cam_across_sub_batch:
            for s in self._metadata:
                min_ncam = min(s['ncam'])
                if sum(s['ncam'] != min_ncam):
                    print('sub-batch has data with different ncam but each same_cam_across_sub_batch=True! Could result in un-even cam sampling')
                    break

        # check batch_size
        assert self._batch_size % self._hparams.sub_batch_size == 0, "sub batches should evenly divide batch_size!"
        # assert np.isclose(sum(self._hparams.splits), 1) and all([0 <= i <= 1 for i in self._hparams.splits]), "splits is invalid"
        assert self._hparams.load_T >=0, "load_T should be non-negative!"

        # smallest max step length of all dataset sources 
        min_steps = min([min(min(m.frame['img_T']), min(m.frame['state_T'])) for m in self._metadata])
        if not self._hparams.load_T:
            self._hparams.load_T = min_steps
        else:
            assert self._hparams.load_T <= min_steps, "ask to load {} steps but some records only have {}!".format(self._hparams.min_T, min_steps)

        mode_sources = [[] for _ in range(len(self.modes))]
        for m_ind, metadata in enumerate(self._metadata):
            files_per_source = self._split_files(m_ind, metadata)
            assert len(files_per_source) == len(self.modes), "files should be split into {} sets (it's okay if sets are empty)".format(len(self.modes))
            for i, (m, fps) in enumerate(zip(mode_sources, files_per_source)):
                _tmp_source = '_'.join(re.split('_', re.split('/', fps[0])[-1])[:2])
                if i == 0:
                    print('{:<16s}'.format(_tmp_source), end=' --> ')
                print(self.modes[i], end=': ')
                print('{:<5d}'.format(len(fps)), end=' ')
                if len(fps):
                    self._random_generator['base'].shuffle(fps)
                    m.append((fps, metadata))  
            print()              

        self._generators = {}
        for name, m in zip(self.modes, mode_sources):
            if m:
                rng = self._random_generator[name]
                mode_source_files, mode_source_metadata = [[t[j] for t in m] for j in range(2)]

                self._generators[name] = self._hdf5_generator(mode_source_files, mode_source_metadata, rng, name)

        if self._hparams.multi_multiprocess:
            self._n_workers = min(self._batch_size, multiprocessing.cpu_count())
            if self._hparams.pool_workers:
                self._n_workers = min(self._hparams.pool_workers, multiprocessing.cpu_count())
            self._n_pool_resets = 0
            self._pool = multiprocessing.Pool(self._n_workers)

    def _split_files(self, source_number, metadata):
        if self._hparams.train_ex_per_source != [-1]:
            return split_train_val_test(metadata, train_ex=self._hparams.train_ex_per_source[source_number], rng=self._random_generator['base'])
        return split_train_val_test(metadata, splits=self._hparams.splits, rng=self._random_generator['base'])

    def _hdf5_generator(self, sources, sources_metadata, rng, mode):
        file_indices, source_epochs = [[0 for _ in range(len(sources))] for _ in range(2)]
        while True:
            file_hparams = [copy.deepcopy(self._hparams) for _ in range(self._batch_size)]
            if self._hparams.RNG:
                file_rng = [rng.getrandbits(64) for _ in range(self._batch_size)]
            else:
                file_rng = [None for _ in range(self._batch_size)]
            
            file_names, file_metadata = [], []
            b = 0
            sources_selected_thus_far = []
            while len(file_names) < self._batch_size:
                # if source_probs is set do a weighted random selection
                if self._hparams.source_probs:
                    selected_source = self._np_rng.choice(len(sources), 1, p=self._hparams.source_probs)[0]
                # if # sources <= # sub_batches then sample each source at least once per batch
                elif len(sources) <= self._batch_size // self._hparams.sub_batch_size and b // self._hparams.sub_batch_size < len(sources):
                    selected_source = b // self._hparams.sub_batch_size
                elif len(sources) > self._batch_size // self._hparams.sub_batch_size:
                    selected_source = rng.randint(0, len(sources) - 1)
                    while selected_source in sources_selected_thus_far:
                        selected_source = rng.randint(0, len(sources) - 1)
                else:
                    selected_source = rng.randint(0, len(sources) - 1)
                sources_selected_thus_far.append(selected_source)

                for sb in range(self._hparams.sub_batch_size):
                    selected_file = sources[selected_source][file_indices[selected_source]]
                    file_indices[selected_source] += 1
                    selected_file_metadata = sources_metadata[selected_source].get_file_metadata(selected_file)
                    file_names.append(selected_file)
                    file_metadata.append(selected_file_metadata)
                    
                    if file_indices[selected_source] >= len(sources[selected_source]):
                        file_indices[selected_source] = 0
                        source_epochs[selected_source] += 1

                        if (mode == 'train') or (mode == 'test') and self._hparams.num_epochs is not None and source_epochs[selected_source] >= self._hparams.num_epochs:
                            break
                        rng.shuffle(sources[selected_source])

                b += self._hparams.sub_batch_size

            if self._hparams.load_random_cam:
                b = 0
                while b < self._batch_size:
                    if self._hparams.same_cam_across_sub_batch:
                        selected_cam = [rng.randint(0, min([file_metadata[b + sb]['ncam'] for sb in range(self._hparams.sub_batch_size)]) - 1)]
                        for sb in range(self._hparams.sub_batch_size):
                            file_hparams[b + sb].cams_to_load = selected_cam
                        b += self._hparams.sub_batch_size
                    else:
                        file_hparams[b].cams_to_load = [rng.randint(0, file_metadata[b]['ncam'] - 1)]
                        b += 1

            batch_jobs = [(fn, fm, fh, fr, self._hparams.color_augmentation, True) for fn, fm, fh, fr in zip(file_names, file_metadata, file_hparams, file_rng)]
            fetch = OrderedDict()   
            
            
            if self._hparams.multi_multiprocess:
                try:
                    batches = self._pool.map_async(_load_data, batch_jobs).get(timeout=self._hparams.pool_timeout)
                except:
                    self._pool.terminate()
                    self._pool.close()
                    self._pool = multiprocessing.Pool(self._n_workers)
                    batches = [_load_data(job) for job in batch_jobs]
            else:
                batches = [_load_data(job) for job in batch_jobs]

            ret_vals = []
            for i, b in enumerate(batches):
                if i == 0:
                    for value in b:
                        ret_vals.append([value[None]])
                else:
                    for v_i, value in enumerate(b):
                        ret_vals[v_i].append(value[None])

            ret_vals = [np.concatenate(v) for v in ret_vals]
            if self._hparams.ret_fnames:
                ret_vals.append(np.array(file_names))
        
            if self._hparams.ret_fnames and self._hparams.load_annotations:
                images, actions, states, annotations, f_names = ret_vals
            elif self._hparams.ret_fnames:
                images, actions, states, f_names = ret_vals
            elif self._hparams.load_annotations:
                images, actions, states, annotations = ret_vals
            else:
                images, actions, states = ret_vals
                                
            fetch['images'] = images
            fetch['actions'] = actions
            fetch['states'] = states
            if self._hparams.ret_fnames:
                fetch['f_names'] = f_names
            if self._hparams.load_annotations:
                fetch['annotations'] = annotations

            yield fetch

    def _init_rng(self):
        # if RNG is not supplied then initialize new RNG
        self._random_generator = {}        
        seeds = [None for _ in range(len(self.modes) + 1)]
        if self._hparams.RNG:
            seeds = [i + self._hparams.RNG for i in range(len(seeds))]
        
        for k, seed in zip(self.modes + ['base'], seeds):
            if k == 'train' and self._hparams.use_random_train_seed:
                seed = None
            self._random_generator[k] = random.Random(seed)
        self._np_rng = np.random.RandomState(self._random_generator['base'].getrandbits(32))
    
    @staticmethod
    def _get_default_hparams():
        default_dict = {
            'RNG': 11381294392481135266,             # random seed to initialize data loader rng
            'use_random_train_seed': False,          # use a random seed for training objects 
            'sub_batch_size': 1,                     # sub batch to sample from each source
            'splits': [0.9, 0.05, 0.05],             # train, val, test
            'num_epochs': None,                      # maximum number of epochs (None if iterate forever)
            'ret_fnames': True,                     # return file names of loaded hdf5 record
            'buffer_size': 100,                      # examples to prefetch
            'all_modes_max_workers': True,           # use multi-threaded workers regardless of the mode
            'load_random_cam': True,                 # load a random camera for each example
            'same_cam_across_sub_batch': False,      # same camera across sub_batches
            'pool_workers': 0,                       # number of workers for pool (if 0 uses batch_size workers)
            'color_augmentation':0.0,                # std of color augmentation (set to 0 for no augmentations)
            'train_ex_per_source': [-1],             # list of train_examples per source (set to [-1] to rely on splits only)
            'pool_timeout': 20,                      # max time to wait to get batch from pool object
            'MAX_RESETS': 10,                        # maximum number of pool resets before error is raised in main thread
            'multi_multiprocess': True,
        }
        for k, v in default_loader_hparams().items():
            default_dict[k] = v
        
        return HParams(**default_dict)
       
    @property
    def batch_size(self):
        return self._batch_size

    @property
    def hparams(self):
        return copy.deepcopy(self._hparams)

    @property
    def modes(self):
        return ['train', 'val', 'test']

    @property
    def generators(self):
        return self._generators
    