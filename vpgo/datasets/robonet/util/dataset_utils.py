import tensorflow as tf
import numpy as np
import matplotlib

def color_augment(image, noise_range=0.2):
    assert noise_range > 0, "noise_range must be positive"
    assert len(list(image.shape)) == 5
    bs = image.shape[0]
    shape = [bs] + [1 for _ in image.shape[1:]]

    min_noise = -noise_range
    max_noise = noise_range
    rand_h = np.random.uniform(low=min_noise, high=max_noise, size=shape)
    rand_s = np.random.uniform(low=min_noise, high=max_noise, size=shape)
    rand_v = np.random.uniform(low=min_noise, high=max_noise, size=shape)
    image_hsv = matplotlib.colors.rgb_to_hsv(image)
    h_, s_, v_ = np.split(image_hsv, 3, -1)
    stack_mod = np.clip(np.concatenate([h_ + rand_h, s_ + rand_s, v_ + rand_v], axis=-1), 0, 1.)
    image_rgb = matplotlib.colors.hsv_to_rgb(stack_mod)
    return image_rgb.astype(np.float32)

def split_train_val_test(metadata, splits=None, train_ex=None, rng=None):
    assert (splits is None) != (train_ex is None), "exactly one of splits or train_ex should be supplied"
    files = metadata.get_shuffled_files(rng)
    train_files, val_files, test_files = None, None, None

    if splits is not None:
        assert len(splits) == 3, "function requires 3 split parameteres ordered (train, val ,test)"
        splits = np.cumsum([int(i * len(files)) for i in splits]).tolist()
    else:
        assert len(files) >= train_ex, "not enough files for train examples!"
        val_split = int(0.5 * (len(files) + train_ex))
        splits = [train_ex, val_split, len(files)]
    
    # give extra fat to val set
    if splits[-1] < len(files):
        diff = len(files) - splits[-1]
        for i in range(1, len(splits)):
            splits[i] += diff
    
    if splits[0]:
        train_files = files[:splits[0]]
    if splits[1]:
        val_files = files[splits[0]: splits[1]]
    if splits[2]:
        test_files = files[splits[1]: splits[2]]
    
    return train_files, val_files, test_files
